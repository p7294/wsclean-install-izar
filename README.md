# Install IDG and WSClean on DAS-6

Create an installation folder and cd it.
Then, set the installation path:

`export INSTALL_DIR=${PWD}`

Load the necessary modules:

`module load izar/gcc/4.8.5/gcc/9.4.0 izar/gcc/9.4.0/git/2.31.1 izar/gcc/9.4.0/git-lfs/2.11.0 izar/gcc/9.4.0/openblas/0.3.18 izar/gcc/9.4.0/boost/1.77.0 izar/gcc/9.4.0/casacore/3.4.0 izar/gcc/9.4.0/cfitsio/4.0.0 izar/gcc/9.4.0/hdf5/1.10.7 izar/gcc/9.4.0/gsl/2.7 izar/gcc/9.4.0/wcslib/7.3 izar/gcc/9.4.0/cmake/3.21.4 izar/gcc/9.4.0/fftw/3.3.10 izar/gcc/9.4.0/python/3.8.12 `

Source environment variables:

`source env.sh`

Build IDG:

`./build_idg.sh`

Build WSClean:

`./build_wsclean.sh`
