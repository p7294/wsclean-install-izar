#!/bin/bash

#Create build idg and build directory
mkdir -p ${INSTALL_DIR}/idg/build
#cd idg folder and clone IDG repository
cd ${INSTALL_DIR}/idg && git clone https://git.astron.nl/RD/idg.git -b 1.0.0
#Configure CMake
cd ${INSTALL_DIR}/idg/build && cmake -DBUILD_LIB_CUDA=OFF -DUSE_LOOKUP_TABLE=ON -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}/idg ../idg
#Make IDG
cd ${INSTALL_DIR}/idg/build && make -j 
#Install IDG
cd ${INSTALL_DIR}/idg/build && make install
