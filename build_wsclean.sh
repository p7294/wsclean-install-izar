#!/bin/bash

#Create build idg and build directory
mkdir -p ${INSTALL_DIR}/wsclean/build
#cd idg folder and clone IDG repository
cd ${INSTALL_DIR}/wsclean && git clone https://gitlab.com/aroffringa/wsclean.git -b v3.1
#Configure CMake
cd ${INSTALL_DIR}/wsclean/build && cmake -DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}/wsclean ../wsclean
#Make IDG
cd ${INSTALL_DIR}/wsclean/build && make -j 
#Install IDG
cd ${INSTALL_DIR}/wsclean/build && make install
