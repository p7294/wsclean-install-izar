#Fix error in WSClean CMake configuration
export OPENBLAS_NUM_THREADS=1
#Add IDG and WSClean library to LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${INSTALL_DIR}/idg/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${INSTALL_DIR}/wsclean/lib
#Add WSClean bin to PATH
export PATH=$PATH:${INSTALL_DIR}/wsclean/bin
